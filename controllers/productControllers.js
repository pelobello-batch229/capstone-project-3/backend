const User = require("../models/User.js");
const Product = require("../models/Product.js");
const auth = require("../auth.js")

// product registration

module.exports.addProduct = (reqBody) => {

	

		let newProduct = new Product({
				productName : reqBody.productName,
				productBrand: reqBody.productBrand,
				description : reqBody.description,
				price : reqBody.price,
				stock : reqBody.stock
			});

			
			return newProduct.save().then((product, error) => {
				
				if (error) {
					return false;
				} else {
					return true;
				};

			});



	

};


// Get all Products

module.exports.getAllProduct = () => {
	return Product.find()
}

//Get all Active Products
module.exports.getAllActiveProduct = () => {
	return Product.find({isActive: true})
}



// Update Product

module.exports.updateProduct = (reqParams, reqBody) => {

	//if(admin.isAdmin){

		console.log(reqParams);
		let updateProduct = {
			productName : reqBody.productName,
			productBrand: reqBody.productBrand,
			description: reqBody.description,
			price: reqBody.price,
			stock: reqBody.stock,
			isActive: reqBody.isActive
		};

		return	Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{
			
			if(error){
				return false;

		
			} else {
				return true
			}
		})

	//} else {
		//return ("Authorized Personnel Only");
	//}

}
// 

module.exports.retrieveOneProduct = (reqParams) => {

	return Product.findById(reqParams.productId)


}

// Archive Product

module.exports.archiveProducts = (reqParams, data) => {
	
	if (data.isAdmin) {
		let archiveProd = {
			isActive: false
		};
		
		return Product.findByIdAndUpdate(reqParams.productId, archiveProd).then((product, error) =>{

			if(error){
				return false;

			
			} else {
				return true
			}
		})
	}else{
		return ("Authorized Personnel Only");
	}
	

}
