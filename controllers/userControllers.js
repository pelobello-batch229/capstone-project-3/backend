const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

// checkemai
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}

// user registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({ 
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	
	return newUser.save().then((user,error) => {
		
		if(error){
			return false;	
		}else{
			
			return true;
		}
	})
}

// User Login
module.exports.userLogin = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;

		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return ("Wrong Password");
			}
		}
	})
}

// User Details
module.exports.getUserDetails = (user) => {

	return User.findById(user.userId).then(result => {
		result.password = ""
		return result
	})
}

// Retrieve ALL USER

module.exports.getAllUser = (user) => {

	if(user.isAdmin){
		return User.find()

	} else {

		return false
	}
		
}

// Add Admin User

module.exports.addAdminUser = (reqParams, admin) =>{

	if(admin.isAdmin){

		let newAdmin = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(reqParams.userId, newAdmin).then((user,err) =>{

			if(err){
				return false;

		
			} else {
				return true
			}
		})
	} else {
		return false
	}
}

module.exports.createOrder = async (reqBody, user) => {

	if(user.isAdmin){
		return ("Customer Only")
	} else {

		let products = await Product.findById(reqBody.productId)
		let {productName, productBrand, price} = products;

		let userOrders = await User.findById(user.userId).then(user => {
	
			let totalAmount = reqBody.quantity * price

			user.orders.push({
					productId: reqBody.productId,
					productName: productName,
					productBrand: productBrand,
					price: price,
					quantity: reqBody.quantity,
					totalAmount: totalAmount
					
				}
			)

			return user.save().then((user,err) =>{
				if(err){
					return false
				} else {
					return true
				}
			})
		})


	let productOrders = await Product.findById(reqBody.productId).then(product => {

			product.orders.push({userId: user.userId},{quantity: reqBody.quantity});

			return product.save().then((product, error) => {
				
				if(error){
					return false;
				
				} else {
					return true;
				}
			})	
		})

		if(userOrders && true){
			return ("Orderdered Successful");
		} else {
			return false;
		}

	} 

}

module.exports.retrieveOrders = (user) => {

	if(user.isAdmin){

	return User.aggregate([{ $match : {isAdmin : false}},{ $group : { _id : "$orders" }}])
			

	} else {
		return ("Authorized Personnel Only")
	}

}

module.exports.retrieveUserOrders = (reqParams,admin) => {

		return User.findById(reqParams.userId).then(result=> {
		let {orders} = result;
	
		return orders
	});
	

}

// module.exports.retrieveUserOrders = (reqParams, admin) => {

// 	// return User.findById(reqParams.userId).then(result=> {
// 	// 	let {orders} = result;
	
// 	// 	return orders
// 	// });
	

// 	// if(admin.isAdmin){
// 	// 	return User.findById(reqParams.userId).then(result=> {
// 	// 	let {orders} = result;
	
// 	// 	return orders
// 	// });
			
// 	// 	return ("user Only")
// 	// } else {
		
// 	// 	return true
		
// 	// }

// 	if(admin.isAdmin){
// 		return false
// 	} else {
// 		return true
// 	}

// }






	







