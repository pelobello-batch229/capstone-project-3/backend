const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product Name is Required!"]
	},
	productBrand:{
		type: String,
		required: [true, "Product Name is Required!"]
	},
	description: {
		type: String,
		required: [true, "Description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
	stock: {
		type: Number,
		required: [true, "Price is required!"]
	},
	
	isActive: {
		type: Boolean,
		default: true
	},
	createdon: {
		type: Date,
		default: new Date()
	},
	orders: [{
		userId: {
			type: String
		},
		orderOn: {
			type: Date,
			default: new Date()
		},
		quantity: {
			type: Number
		}

	}]

})

module.exports = mongoose.model("Product", productSchema);