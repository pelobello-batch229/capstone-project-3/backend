const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false,
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required!"]
	},
	orders: [{
			productId: {
				type: String,
			},
			productName: {
				type: String	
			},
			productBrand: {
				type: String
			},
			price:{
				type: Number,
			},
			quantity:{
				type: Number			
			},
			totalAmount: {
				type: Number
			},
			purchaseOn: {
				type: Date,
				default: new Date()
			}

		}]

})



module.exports = mongoose.model("User", userSchema)

