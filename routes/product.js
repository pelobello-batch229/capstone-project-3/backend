const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth");

// add prodcuts //  admin user only
router.post("/add", (req,res) =>{
// auth.verify,
    //  const admin = {
    //     isAdmin: auth.decode(req.headers.authorization).isAdmin
    // }

	productControllers.addProduct(req.body).then(controllerResult => res.send(controllerResult));
})


// get all products
router.get("/all", (req, res) => {
    productControllers.getAllProduct().then(controllerResult => {
        res.send(controllerResult)
    });
})

// get all  Active products
router.get("/active", (req, res) => {

    productControllers.getAllActiveProduct().then(controllerResult => {
        res.send(controllerResult)
    });
})


// get Single products
router.get("/:productId", (req, res) => {

    productControllers.retrieveOneProduct(req.params).then(controllerResult => {
        res.send(controllerResult)
    });
})

// update product by id // admin user only
router.put("/:productId",  (req,res) => {

        // const admin = {
        //   isAdmin: auth.decode(req.headers.authorization).isAdmin
        // } 

    productControllers.updateProduct(req.params, req.body).then(controllerResult => res.send(controllerResult));
})


//Archive Product admin user only
router.put("/:productId/archive", auth.verify, (req,res) => {

      const admin = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productControllers.archiveProducts(req.params, admin).then(resultFromController => res.send(resultFromController));
})

module.exports = router;